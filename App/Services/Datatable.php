<?php

namespace App\Services;

use PDO;
use PDOException;

class Datatable extends SSP
{
    /**
     * Database connection
     *
     * Obtain an PHP PDO connection from a connection details array
     *
     * @param  array $conn SQL connection details. The array should have
     *                     the following properties
     *                     * host - host name
     *                     * db   - database name
     *                     * user - user name
     *                     * pass - user password
     *
     * @return resource PDO connection
     */
    public static function db($conn)
    {
        return static::sql_connect($conn);
    }

    /**
     * Connect to the database
     *
     * @param  array $sql_details SQL server connection details array, with the
     *                            properties:
     *                            * host - host name
     *                            * db   - database name
     *                            * user - user name
     *                            * pass - user password
     *
     * @return resource Database connection handle
     */
    public static function sql_connect($sql_details)
    {
        try {
            $db = @new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASS, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            self::fatal("An error occurred while connecting to the database. "."The error reported by the server was: ".$e->getMessage());
        }

        return $db;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @param bool   $isJoin  Determine the the JOIN/complex query or simple one
     *
     * @return string SQL order by clause
     */
    static function order($request, $columns, $isJoin = false)
    {
        $order = '';

        if (isset($request['order']) && count($request['order'])) {
            $orderBy = [];
            $dtColumns = SSP::pluck($columns, 'dt');

            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];

                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];

                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ? 'ASC' : 'DESC';

                    $orderBy[] = ($isJoin) ? (is_numeric($column['dt']) ? $column['db'] : $column['dt']).' '.$dir : '`'.(is_numeric($column['dt']) ? $column['db'] : $column['dt']).'` '.$dir;
                }
            }

            $order = 'ORDER BY '.implode(', ', $orderBy);
        }

        return $order;
    }
}