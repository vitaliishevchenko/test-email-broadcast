##Installation:

If database is not set up, execute scripts located in `database` folder

Database config is located at `config\database.php`


The easiest and secure way to install a module into your project - is simply to configure Apache / Nginx, specifying the `public/index.php` as the root path, and then routing do all stuff

Other way - to merge yours index.php, and update `App/routes.php` to preferred paths
