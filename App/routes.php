<?php

App\Services\Router::serve([
    "/"                    => App\Http\HomeController::class,
    "/email-stats"         => App\Http\EmailStatsController::class,
    "/broadcast-stats"     => App\Http\BroadcastStatsController::class,
    "/rss-broadcast-stats" => App\Http\RssBroadcastStatsController::class,
    "/data-table"          => App\Http\DatatableController::class,
]);