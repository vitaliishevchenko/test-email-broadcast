<?php

namespace App\Models;

use App\Services\Datatable;

class Email extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'emails';

    /*
     * The model's columns
     *
     * @var array
     */
    protected $columns = [
        'id',
        'cid',
        'pid',
        'sender_profile_id',
        'name',
        'subject',
        'altSubject',
        'body',
        'date',
        'archive',
    ];

    /**
     * Email constructor.
     *
     * @param null  $startDate
     * @param null  $endDate
     * @param array $excludedIds
     * @param null  $cid
     */
    public function __construct($startDate = null, $endDate = null, $excludedIds = [], $cid = null)
    {
        parent::__construct();
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->excludedIds = $excludedIds;
        $this->cid = $cid;
    }

    /**
     * Get sent emails
     *
     * @return array|bool
     */
    public function getSentEmails()
    {
        $data = $this->connection->select('queue_emails', ['[>]emails' => ['email_id' => 'id']], [
            'queue_emails.id(queue_emails_id)',
            'queue_emails.email_id',
            'queue_emails.date',
        ], array_merge(['queue_emails.sent' => '1'], $this->prepareGetWhereQuery('queue_emails')));

        return $data;
    }

    /**
     * Count sent emails
     *
     * @return int
     */
    public function countSentEmails()
    {
        return $this->connection->count('queue_emails', ['[>]emails' => ['email_id' => 'id']], ['queue_emails.id'], array_merge(['queue_emails.sent' => '1'], $this->prepareCountWhereQuery('queue_emails')));
    }

    /**
     * @return array|bool
     */
    public function getOpenedEmails()
    {
        $data = $this->connection->select('email_opens', ['[>]emails' => ['email_id' => 'id']], [
            'email_opens.id(email_opens_id)',
            'email_opens.email_id',
            'email_opens.date',
        ], array_merge([], $this->prepareGetWhereQuery('email_opens')));

        return $data;
    }

    /**
     * @param bool $unique
     *
     * @return int
     */
    public function countOpenedEmails($unique = false)
    {
        if ($unique) {
            $data = $this->connection->select('email_opens', [
                '[>]emails' => ['email_id' => 'id'],
            ], ['email_opens.id'], array_merge($this->prepareCountWhereQuery('email_opens'), [
                'GROUP' => [
                    'email_opens.email_id',
                    'email_opens.cc_id',
                ],
            ]));

            return count($data);
        }

        return $this->connection->count('email_opens', [
            '[>]emails' => ['email_id' => 'id'],
        ], [
            'email_opens.id',
        ], $this->prepareCountWhereQuery('email_opens'));
    }

    /**
     * @return array|bool
     */
    public function getClickedEmails()
    {
        $data = $this->connection->select('email_clicks', ['[>]emails' => ['email_id' => 'id']], [
            'email_clicks.id(email_clicks_id)',
            'email_clicks.email_id',
            'email_clicks.date',
        ], $this->prepareGetWhereQuery('email_clicks'));

        return $data;
    }

    /**
     * @param bool $unique
     *
     * @return int
     */
    public function countClickedEmails($unique = false)
    {
        if ($unique) {
            $data = $this->connection->select('email_clicks', ['[>]emails' => ['email_id' => 'id']], ['email_clicks.id'], array_merge($this->prepareCountWhereQuery('email_clicks'), [
                'GROUP' => [
                    'email_clicks.email_id',
                    'email_clicks.cc_id',
                ],
            ]));

            return count($data);
        }

        return $this->connection->count('email_clicks', ['[>]emails' => ['email_id' => 'id']], ['email_clicks.id'], $this->prepareCountWhereQuery('email_clicks'));
    }

    /**
     * Datatables presentation of model
     *
     * @return array
     */
    public function datatable()
    {
        $columns = [
            [
                'db'    => 'emails.id',
                'dt'    => 'id',
                'field' => 'id',
            ],
            [
                'db'    => 'emails.name',
                'dt'    => 'name',
                'field' => 'name',
            ],
            [
                'db'    => 'emails.subject',
                'dt'    => 'subject',
                'field' => 'subject',
            ],
            [
                'db'    => 'emails.date as email_date',
                'dt'    => 'email_date',
                'field' => 'email_date',
            ],
            [
                'db'    => 'COUNT(distinct `qe`.`id`) AS sent_count',
                'dt'    => 'sent_count',
                'field' => 'sent_count',
            ],
            [
                'db'    => 'COUNT(distinct `ec`.`cc_id`) AS click_count_unique',
                'dt'    => 'click_count_unique',
                'field' => 'click_count_unique',
            ],
            [
                'db'    => 'COUNT(distinct `ec`.`id`) AS click_count',
                'dt'    => 'click_count',
                'field' => 'click_count',
            ],
            [
                'db'    => 'COUNT(distinct `eo`.`cc_id`) AS open_count_unique',
                'dt'    => 'open_count_unique',
                'field' => 'open_count_unique',
            ],
            [
                'db'    => 'COUNT(distinct `eo`.`id`) AS open_count',
                'dt'    => 'open_count',
                'field' => 'open_count',
            ],
            [
                'db'        => 'COUNT(distinct `eo`.`cc_id`) / COUNT(distinct `qe`.`id`) AS open_percent',
                'dt'        => 'open_percent',
                'field'     => 'open_percent',
                'formatter' => function ($d) {
                    return number_format($d, 2).'%';
                },
            ],
            [
                'db'        => 'COUNT(distinct `ec`.`cc_id`) / COUNT(distinct `qe`.`id`) AS click_percent',
                'dt'        => 'click_percent',
                'field'     => 'click_percent',
                'formatter' => function ($d) {
                    return number_format($d, 2).'%';
                },
            ],

        ];

        $where = array_merge(['qe.sent = 1'], $this->startDate ? ['qe.date > '.$this->startDate] : [], $this->endDate ? ['qe.date <'.$this->endDate] : [], $this->cid ? ['emails.cid ='.$this->cid] : []);

        $joinQuery = 'FROM `emails`
                        LEFT JOIN `queue_emails` AS `qe` ON (`qe`.`email_id` = `emails`.`id`)
                        LEFT JOIN `email_clicks` AS `ec` ON (`ec`.`email_id` = `emails`.`id`)
                        LEFT JOIN `email_opens` AS `eo` ON (`eo`.`email_id` = `emails`.`id`)';
        $groupBy = '`emails`.`id`';

        $data = Datatable::simple($_GET, [], $this->table, $this->primaryKey, $columns, $joinQuery, implode(' AND ', $where), $groupBy);

        return $data;
    }
}