<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once __DIR__.'/../Psr4AutoloaderClass.php';

$loader = new \Psr4AutoloaderClass;
// register the autoloader
$loader->register();

// register the base directories for the namespace prefix
$loader->addNamespace('App', __DIR__.'/../App');

$app = require_once __DIR__.'/../App/app.php';
