<?php

namespace App\Http;

class HomeController extends Controller
{
    /**
     * Show view template
     *
     * @return void
     */
    public function get()
    {
        view('index');
    }
}