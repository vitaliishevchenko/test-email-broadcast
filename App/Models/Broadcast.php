<?php

namespace App\Models;

use App\Services\Datatable;

class Broadcast extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'broadcasts';

    /*
     * The model's columns
     *
     * @var array
     */
    protected $columns = [
        'id',
        'cid',
        'pid',
        'email_id',
        'name',
        'sent_to',
        'send_type',
        'send_timing',
        'btype',
        'rss_url',
        'rss_last_sent',
        'date',
        'cronned',
        'excluded_list',
        'archive',
        'sender_profile_id',
    ];

    /**
     * Defines that current model is used for RSS
     *
     * @var bool
     */
    protected $rss = false;

    /**
     * Broadcast constructor.
     *
     * @param bool  $rss
     * @param null  $startDate
     * @param null  $endDate
     * @param array $excludedIds
     * @param null  $cid
     */
    public function __construct($startDate = null, $endDate = null, $excludedIds = [], $cid = null, $rss = false)
    {
        parent::__construct();

        $this->rss = $rss;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->excludedIds = $excludedIds;
        $this->cid = $cid;
    }

    /**
     * Get all broadcasts
     * Also scoped by all additional where conditions
     *
     * @return array|bool
     */
    public function getBroadcasts()
    {
        $data = $this->connection->select('broadcasts', [
            '[>]emails'                    => ['email_id' => 'id'],
            '[><]reports_email_history(h)' => ['id' => 'source_id'],
        ], [
            'broadcasts.id(broadcast_id)',
            'broadcasts.cid',
            'broadcasts.pid',
            'broadcasts.email_id',
            'broadcasts.date',
        ], array_merge(['h.source' => 'broadcasts'], $this->rss ? ['btype' => 'rss'] : [], $this->prepareCountWhereQuery('broadcasts')));

        $this->broadcasts = $data;

        return $data;
    }

    /**
     * Get sent broadcasts info
     *
     * @return array|bool
     */
    public function getSentEmails()
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();
        $data = $this->connection->select('queue_emails', [
            '[>]broadcasts' => ['email_id' => 'email_id'],
            '[>]emails'     => ['email_id' => 'id'],
        ], [
            'queue_emails.id(queue_emails_id)',
            'queue_emails.email_id',
            'queue_emails.date',
        ], array_merge([
            'queue_emails.sent'     => '1',
            'queue_emails.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareGetWhereQuery('queue_emails')));

        return $data;
    }

    /**
     * Count sent broadcasts
     *
     * @return int
     */
    public function countSentEmails()
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();

        $data = $this->connection->count('queue_emails', [
            '[>]broadcasts' => ['email_id' => 'email_id'],
            '[>]emails'     => ['email_id' => 'id'],
        ], ['queue_emails.id'], array_merge([
            'queue_emails.sent'     => '1',
            'queue_emails.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareCountWhereQuery('queue_emails')));

        return $data;
    }

    /**
     * Get open broadcasts info data
     *
     * @return array
     */
    public function getOpenedEmails()
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();
        $data = $this->connection->select('email_opens', [
            '[>]broadcasts' => ['email_id' => 'email_id'],
            '[>]emails'     => ['email_id' => 'id'],
        ], [
            'email_opens.id(email_opens_id)',
            'email_opens.email_id',
            'email_opens.date',
        ], array_merge([
            'email_opens.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareGetWhereQuery('email_opens')));

        return $data;
    }

    /**
     * Count open broadcasts
     *
     * @param bool $unique
     *
     * @return int
     */
    public function countOpenedEmails($unique = false)
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();

        if ($unique) {
            $data = $this->connection->count('email_opens', [
                '[>]broadcasts'                => ['email_id' => 'email_id'],
                '[>]emails'                    => ['email_id' => 'id'],
                '[><]reports_email_history(h)' => ['his_id' => 'id'],
            ], [$unique ? 'email_opens.cc_id' : 'email_opens.id'], array_merge([
                'email_opens.email_id' => array_pluck($broadcasts, 'email_id'),
            ], $this->prepareCountWhereQuery('email_opens'), [
                'GROUP' => [
                    'email_opens.email_id',
                    'email_opens.cc_id',
                ],
            ]));

            return count($data);
        }

        return $this->connection->count('email_opens', [
            '[>]broadcasts'                => ['email_id' => 'email_id'],
            '[>]emails'                    => ['email_id' => 'id'],
            '[><]reports_email_history(h)' => ['his_id' => 'id'],
        ], [$unique ? 'email_opens.cc_id' : 'email_opens.id'], array_merge([
            'email_opens.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareCountWhereQuery('email_opens')));
    }

    /**
     * Get broadcasts info data about clicks
     *
     * @return array|bool
     */
    public function getClickedEmails()
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();
        $data = $this->connection->select('email_clicks', [
            '[>]broadcasts' => ['email_id' => 'email_id'],
            '[>]emails'     => ['email_id' => 'id'],
        ], [
            'email_clicks.id(email_opens_id)',
            'email_clicks.email_id',
            'email_clicks.date',
        ], array_merge([
            'email_clicks.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareGetWhereQuery('email_clicks')));

        return $data;
    }

    /**
     * Count clicked broadcasts
     *
     * @param bool $unique
     *
     * @return int
     */
    public function countClickedEmails($unique = false)
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();

        if ($unique) {
            $data = $this->connection->select('email_clicks', [
                '[>]broadcasts'                => ['email_id' => 'email_id'],
                '[>]emails'                    => ['email_id' => 'id'],
                '[><]reports_email_history(h)' => ['his_id' => 'id'],
            ], ['email_clicks.id'], array_merge([
                'email_clicks.email_id' => array_pluck($broadcasts, 'email_id'),
            ], $this->prepareCountWhereQuery('email_clicks'), [
                'GROUP' => [
                    'email_clicks.email_id',
                    'email_clicks.cc_id',
                ],
            ]));

            return count($data);
        }

        return $this->connection->count('email_clicks', [
            '[>]broadcasts'                => ['email_id' => 'email_id'],
            '[>]emails'                    => ['email_id' => 'id'],
            '[><]reports_email_history(h)' => ['his_id' => 'id'],
        ], ['email_clicks.id'], array_merge([
            'email_clicks.email_id' => array_pluck($broadcasts, 'email_id'),
        ], $this->prepareCountWhereQuery('email_clicks')));
    }

    /**
     * Datatables presentation of model
     *
     * @return array
     */
    public function datatable()
    {
        $broadcasts = isset($this->broadcasts) ? $this->broadcasts : $this->getBroadcasts();

        $columns = [
            [
                'db'    => 'emails.id',
                'dt'    => 'id',
                'field' => 'id',
            ],
            [
                'db'    => 'emails.name',
                'dt'    => 'name',
                'field' => 'name',
            ],
            [
                'db'    => 'emails.subject',
                'dt'    => 'subject',
                'field' => 'subject',
            ],
            [
                'db'    => 'emails.date as email_date',
                'dt'    => 'email_date',
                'field' => 'email_date',
            ],
            [
                'db'    => 'COUNT(distinct `qe`.`id`) AS sent_count',
                'dt'    => 'sent_count',
                'field' => 'sent_count',
            ],
            [
                'db'    => 'COUNT(distinct `ec`.`cc_id`) AS click_count_unique',
                'dt'    => 'click_count_unique',
                'field' => 'click_count_unique',
            ],
            [
                'db'    => 'COUNT(distinct `ec`.`id`) AS click_count',
                'dt'    => 'click_count',
                'field' => 'click_count',
            ],
            [
                'db'    => 'COUNT(distinct `eo`.`cc_id`) AS open_count_unique',
                'dt'    => 'open_count_unique',
                'field' => 'open_count_unique',
            ],
            [
                'db'    => 'COUNT(distinct `eo`.`id`) AS open_count',
                'dt'    => 'open_count',
                'field' => 'open_count',
            ],
            [
                'db'        => 'COUNT(distinct `eo`.`cc_id`) / COUNT(distinct `qe`.`id`) AS open_percent',
                'dt'        => 'open_percent',
                'field'     => 'open_percent',
                'formatter' => function ($d) {
                    return number_format($d, 2).'%';
                },
            ],
            [
                'db'        => 'COUNT(distinct `ec`.`cc_id`) / COUNT(distinct `qe`.`id`) AS click_percent',
                'dt'        => 'click_percent',
                'field'     => 'click_percent',
                'formatter' => function ($d) {
                    return number_format($d, 2).'%';
                },
            ],

        ];

        $where = array_merge(['qe.sent = 1'], ["emails.id IN ('".implode("','", array_pluck($broadcasts, 'email_id'))."')"], $this->startDate ? ['qe.date > '.$this->startDate] : [], $this->endDate ? ['qe.date <'.$this->endDate] : [], $this->cid ? ['emails.cid ='.$this->cid] : []);

        $joinQuery = 'FROM `emails`
                        LEFT JOIN `queue_emails` AS `qe` ON (`qe`.`email_id` = `emails`.`id`)
                        LEFT JOIN `email_clicks` AS `ec` ON (`ec`.`email_id` = `emails`.`id`)
                        LEFT JOIN `email_opens` AS `eo` ON (`eo`.`email_id` = `emails`.`id`)';
        $groupBy = '`emails`.`id`';

        $data = Datatable::simple($_GET, [], $this->table, $this->primaryKey, $columns, $joinQuery, implode(' AND ', $where), $groupBy);

        return $data;
    }
}