<?php

namespace App\Models;

use App\Services\Medoo;

abstract class Model
{
    /**
     * The connection name for the model.
     *
     * @var \App\Services\Medoo
     */
    protected $connection;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /*
     * The model's columns
     *
     * @var array
     */
    protected $columns = ['id'];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Start date filter property
     *
     * @var int|null
     */
    protected $startDate;

    /**
     * End date filter property
     *
     * @var int|null
     */
    protected $endDate;

    /**
     * Company id filter property
     *
     * @var int|null
     */
    protected $cid;

    /**
     * Excluded ids filter property
     *
     * @var int|null
     */
    protected $excludedIds = [];

    /**
     * Create a new model instance.
     *
     * @param  array $attributes
     */
    public function __construct($attributes = [])
    {
        $this->connection = new Medoo([
            'database_type' => 'mysql',
            'database_name' => DB_NAME,
            'server'        => DB_HOST,
            'username'      => DB_USER,
            'password'      => DB_PASS,
        ]);

        $this->attributes = $attributes;
    }

    /**
     * @return array
     */
    public function all()
    {
        $models = $this->connection->select($this->table, '*');

        return static::listToModels($models);
    }

    /**
     * @param array $list
     *
     * @return array
     */
    public static function listToModels(array $list = [])
    {
        foreach ($list as $key => $model) {
            $list[$key] = new static($model);
        }

        return $list;
    }

    /**
     * Convert model to array
     *
     * @return array
     */
    public function toArray()
    {
        return $this->getAttributes();
    }

    /**
     * Get model attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        $model = [];
        foreach ($this->attributes as $key => $attribute) {
            if (is_numeric($attribute)) {
                $model[$key] = (int) $attribute;
            } else {
                $model[$key] = $attribute;
            }
        }

        return $model;
    }

    /**
     * @return bool|int|mixed
     */
    public function count()
    {
        return $this->connection->count($this->table);
    }

    /**
     * @param string $table
     *
     * @return array
     */
    protected function prepareGetWhereQuery($table = 'emails')
    {
        $where = $this->prepareCountWhereQuery($table);
        $where = array_merge($where, ! empty($this->excludedIds) ? ['email_id[!]' => $this->excludedIds] : []);

        return $where;
    }

    /**
     * @param string $table
     *
     * @return array
     */
    protected function prepareCountWhereQuery($table = 'emails')
    {
        $where = [];

        $where = array_merge($where, $this->startDate ? [$table.'.date[>]' => $this->startDate] : []);
        $where = array_merge($where, $this->endDate ? [$table.'.date[<]' => $this->endDate] : []);
        $where = array_merge($where, $this->cid ? [($this instanceof Broadcast ? 'broadcasts' : 'emails').'.cid' => $this->cid] : []);

        return $where;
    }
}