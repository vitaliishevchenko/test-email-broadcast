<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"-->
    <!--      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link href="/vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.16.0/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<div class="container-fluid">
    <header class="row">
        <div class="col-xs-12">
            <h3 class="title">
                Email Reports
            </h3>
        </div>
    </header>
    <div class="row content">
        <div class="col-xs-12">
            <?php include __DIR__.'/includes/widgets.php';?>
            <?php include __DIR__.'/includes/graphs.php';?>
            <?php include __DIR__.'/includes/data-tables.php';?>
            <div class="row">
                <div class="col-md-12">
                    <div class="btn btn-primary ajax">Refresh</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js"></script>
<!--<script src="https://cdn.zingchart.com/zingchart.min.js"></script>-->
<script src="/vendor/js/zingchart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.16.0/jquery.daterangepicker.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>