'use strict'
let currentWidget = null
let startDate = (moment().startOf('month').valueOf() / 1000).toFixed(0)
let endDate = (moment().endOf('day').valueOf() / 1000).toFixed(0)
let dataTable
let excludedIds = new Set([])
let cid = null

$(document).ready(() => {
  'use strict'

  let dateRange = $('#date-picker')
  let table = $('.data-table #data-table')
  let runBtn = $('.ajax')
  let cidBlock = $('#cid')
  let emailWidget = new Widget('email')
  currentWidget = emailWidget
  let broadcastWidget = new Widget('broadcast')
  let rssBroadcastWidget = new Widget('rss-broadcast')

  runBtn.on('click', () => {
    reload()
  })

  cidBlock.on('input blur', (e) => {
    cid = e.target.value
    if (!isNaN(parseFloat(cid)) && isFinite(cid)) {
      reload()
    } else {
      cid = null
    }
  })

  zingchart.render({
    id: 'zing-chart',
    data: emailWidget.chartConfig,
  })

  dataTable = table.DataTable({
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: '/data-table',
      data: function (data) {
        return Object.assign(data, {
          startDate,
          endDate,
          currentWidget: currentWidget.name,
          cid
        })
      }
    },
    columns: [
      {
        data: 'id',
        title: '',
        render: (data, display, row) => ('<input type="checkbox" class="email-sent" name="ids[' + data + ']" ' + (excludedIds.has(data) ? '' : 'checked') + '>')
      },
      {
        title: 'Date',
        data: 'email_date',
        render: (data) => (moment(data * 1000).format('DD MMM Y')),
        searchable: false
      },
      {title: 'Name', data: 'name'},
      {title: 'Subject', data: 'subject'},
      {title: 'Sent', data: 'sent_count', searchable: false},
      {
        title: 'Opened',
        data: 'open_count',
        searchable: false,
        render: (data, display, row) => (row.open_count_unique + ' / ' + data)
      },
      {
        title: 'Clicked',
        data: 'click_count',
        searchable: false,
        render: (data, display, row) => (row.click_count_unique + ' / ' + data)
      },
      {title: 'Open(%)', data: 'open_percent', searchable: false},
      {title: 'CTR(%)', data: 'click_percent', searchable: false},
    ],
  })

  table.on('change', '.email-sent', (e) => {
    let id = e.target.name.replace('ids[', '').replace(']', '')
    if (e.target.checked) {
      excludedIds.delete(id)
    } else {
      excludedIds.add(id)
    }

    emailWidget.update()
    broadcastWidget.update()
    rssBroadcastWidget.update()
  })

  dateRange.val(moment().startOf('month').format('Y-MM-DD') + ' to ' + moment().endOf('day').format('Y-MM-DD'))

  dateRange
    .dateRangePicker({
      inline: true,
      container: '#date-picker-container',
      alwaysOpen: true,
    })
    .bind('datepicker-change', function (event, obj) {
      let date1 = moment(obj.date1.getTime()).startOf('day').valueOf()
      let date2 = moment(obj.date2.getTime()).endOf('day').valueOf()
      excludedIds = new Set([])
      startDate = (date1 / 1000).toFixed(0)
      endDate = (date2 / 1000).toFixed(0)
      reload()
    })

  function reload () {
    emailWidget.update()
    broadcastWidget.update()
    rssBroadcastWidget.update()
    dataTable.ajax.reload()
  }

  reload()
})

class Widget {
  constructor (name) {
    this.name = name + '-stats'
    this._sent_count = 0
    this._open_count = 0
    this._click_count = 0
    this._sent_emails = []
    this._open_emails = []
    this._click_emails = []

    this.element = document.getElementById(this.name)
    this.sent_counter = this.element.getElementsByClassName('sent-counter')[0]
    this.open_counter = this.element.getElementsByClassName('open-counter')[0]
    this.click_counter = this.element.getElementsByClassName('click-counter')[0]
    this.widget_open = this.element.getElementsByClassName('widget-open')[0]
    this.widget_ctr = this.element.getElementsByClassName('widget-ctr')[0]

    this.element.addEventListener('click', () => {
      let activeWidgets = document.getElementsByClassName('email-widget-block active')
      let forEach = Array.prototype.forEach
      forEach.call(activeWidgets, function (element) {
        element.classList.remove('active')
      })
      currentWidget = this
      excludedIds = new Set([])
      this.fetchEmails()
      dataTable.ajax.reload()
      this.element.classList.add('active')
    })

    this.chartConfig = {
      'globals': {
        'font-family': 'Roboto'
      },
      'graphset': [
        {
          'type': 'area',
          'background-color': '#fff',
          'utc': false,
          'plotarea': {
            'margin-top': '10%',
            'margin-right': 'dynamic',
            'margin-bottom': 'dynamic',
            'margin-left': 'dynamic',
            'adjust-layout': true
          },
          'scale-x': {
            'min-value': startDate ? startDate * 1000 : null,
            'max-value': endDate ? endDate * 1000 : null,
            'step': '1minute',
            'transform': {
              'type': 'date',
              'all': '%m/%d/%Y<br>%H:%i:%s'
            },
            'label': {
              'text': 'Date Range',
              'font-size': '14px',
              'font-weight': 'normal',
              'offset-x': '10%',
              'font-angle': 360
            },
            'item': {
              'text-align': 'center',
              'font-color': '#05636c'
            },
            'zooming': 1,
            'max-labels': 7,
            // "max-items": 12,
            'items-overlap': false,
            'tick': {
              'line-width': '1px'
            },
          },
          'scale-y': {
            'item': {
              'font-color': '#05636c',
              'font-weight': 'normal'
            },
            'label': {
              'text': 'Emails',
              'font-size': '14px'
            },
            'guide': {
              'line-width': '0px',
              'alpha': 0.2,
              'line-style': 'dashed'
            }
          },
          'plot': {
            'line-width': 2,
            'marker': {
              'size': 4,
              'visible': true
            },
            'tooltip': {
              'font-family': 'Roboto',
              'font-size': '15px',
              'text': '%v %t',
              'text-align': 'left',
              'border-radius': 5,
              'padding': 10,
            }
          },
          'series': []
        }
      ]
    }

    console.log('Define ' + this.name + ' widget')
  }

  get emails () {
    return [...new Set(this._sent_emails.pluck('email_id'))]
  }

  get sent_emails () {
    return this._sent_emails
  }

  get open_emails () {
    return this._open_emails
  }

  get click_emails () {
    return this._click_emails
  }

  set sent_emails (value) {
    this._sent_emails = value
  }

  set open_emails (value) {
    this._open_emails = value
  }

  set click_emails (value) {
    this._click_emails = value
  }

  get sent_count () {
    return this._sent_count
  }

  set sent_count (value) {
    this.sent_counter.textContent = (value).toLocaleString()
    this._sent_count = value
  }

  get open_count () {
    return this._open_count
  }

  set open_count (value) {
    this.open_counter.textContent = (value).toLocaleString()
    this._open_count = value
    if (this.sent_count > 0) {
      this.widget_open.innerHTML = 'Open <strong>' + (this.open_count_unique / this.sent_count).toFixed(2) + '%</strong>'
    } else {
      this.widget_open.innerHTML = 'Open <strong>0.00%</strong>'
    }
  }

  get click_count () {
    return this._click_count
  }

  set click_count (value) {
    this.click_counter.textContent = (value).toLocaleString()
    this._click_count = value

    if (this.sent_count > 0) {
      this.widget_ctr.innerHTML = 'CTR <strong>' + (this.click_count_unique / this.sent_count).toFixed(2) + '%</strong>'
    } else {
      this.widget_ctr.innerHTML = 'CTR <strong>0.00%</strong>'
    }
  }

  updateChart () {
    Object.assign(this.chartConfig.graphset[0]['scale-x'], {
      'min-value': startDate ? startDate * 1000 : null,
      'max-value': endDate ? endDate * 1000 : null,
    })
    this.chartConfig.graphset[0].series = []

    this.pushEmailsToChart('sent_emails', this.sent_emails)
    this.pushEmailsToChart('open_emails', this.open_emails)
    this.pushEmailsToChart('click_emails', this.click_emails)
  }

  pushEmailsToChart (emailsName, emails) {
    let lineColor
    let bgColor
    let tooltipName
    let groups = emails.groupBy('date')
    let values = []

    switch (emailsName) {
      case 'open_emails':
        lineColor = '#66c2a5'
        bgColor = '#66c2a5'
        tooltipName = 'Opened emails'
        break
      case 'click_emails':
        lineColor = '#8da0cb'
        bgColor = '#8da0cb'
        tooltipName = 'Clicked emails'
        break
      default:
        lineColor = '#fc8d62'
        bgColor = '#fc8d62'
        tooltipName = 'Sent emails'
        break
    }

    for (let date in groups) {
      if (groups.hasOwnProperty(date)) {
        values.push([date * 1000, groups[date].length])
      }
    }

    this.chartConfig.graphset[0].series.push({
      values,
      'line-color': lineColor,
      'aspect': 'spline',
      'background-color': bgColor,
      'alpha-area': '.3',
      'font-family': 'Roboto',
      'font-size': '14px',
      'text': tooltipName
    })

    zingchart.exec('zing-chart', 'setdata', {
      data: this.chartConfig
    })
  }

  fetchEmails () {
    $.get('/' + this.name + '?fetch', {startDate, endDate, cid, excludedIds: [...excludedIds]}).then(response => {
      Object.assign(this, response.data)
      this.updateChart()
    })
  }

  update () {
    $.get('/' + this.name, {startDate, endDate, cid, excludedIds: [...excludedIds]}).then(response => {
      Object.assign(this, response.data)
    })
    if (currentWidget.name === this.name) {
      this.fetchEmails()
    }
  }
}

// Helpers
Array.prototype.groupBy = function (key) {
  return this.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x)
    return rv
  }, {})
}

Array.prototype.pluck = function (key) {
  return this.map(function (object) { return object[key] })
}