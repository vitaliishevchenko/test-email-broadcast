<?php

namespace App\Http;

use App\Models\Model;

class Controller
{
    /**
     * Output prepared data as server response
     *
     * @param array $data
     */
    public function responseXhr($data = [])
    {
        if (is_array($data)) {
            $data = $this->formatResponse($data);
        }

        echo $this->safe_json_encode(['data' => $data]);
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function formatResponse($data)
    {
        foreach ($data as $key => $value) {
            if ($value instanceof Model) {
                $data[$key] = $value->toArray();
            } else {
                if (is_array($value)) {
                    $data[$key] = $this->formatResponse($value);
                }
            }
        }

        return $data;
    }

    public function safe_json_encode($value, $options = 0, $depth = 512)
    {
        $encoded = json_encode($value, $options, $depth);
        if ($encoded === false && $value && json_last_error() == JSON_ERROR_UTF8) {
            $encoded = json_encode($this->utf8ize($value), $options, $depth);
        }

        return $encoded;
    }

    public function utf8ize($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, 'UTF-8', 'UTF-8');
        }

        return $mixed;
    }

    /**
     * @return string|null
     */
    public function getStartDate()
    {
        return ! empty($_GET['startDate']) ? $_GET['startDate'] : null;
    }

    /**
     * @return string|null
     */
    public function getEndDate()
    {
        return ! empty($_GET['endDate']) ? $_GET['endDate'] : null;
    }

    /**
     * @return string|null
     */
    public function getCompanyId()
    {
        return ! empty($_GET['cid']) ? $_GET['cid'] : null;
    }

    /**
     * @return array
     */
    public function getExcludedIds()
    {
        return ! empty($_GET['excludedIds']) && is_array($_GET['excludedIds']) ? $_GET['excludedIds'] : [];
    }
}