<?php

namespace App\Http;

use App\Models\Broadcast;
use App\Models\Email;

class DatatableController extends Controller
{
    public function get_xhr()
    {
        $currentWidget = ! empty($_GET['currentWidget']) ? $_GET['currentWidget'] : null;

        $startDate = $this->getStartDate();
        $endDate = $this->getEndDate();
        $cid = $this->getCompanyId();

        switch ($currentWidget) {
            case 'broadcast-stats':
                $model = new Broadcast($startDate, $endDate, [], $cid);
                break;
            case 'rss-broadcast-stats':
                $model = new Broadcast($startDate, $endDate, [], $cid, true);
                break;
            default:
                $model = new Email($startDate, $endDate, [], $cid);
                break;
        }

        $data = $model->datatable();

        echo json_encode($data);
    }
}