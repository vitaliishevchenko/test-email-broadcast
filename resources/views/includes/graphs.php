<div class="row graph">
    <div class="col-md-8">
        <div id="zing-chart"></div>
    </div>
    <div class="col-md-4">
        <input id="date-picker" type="hidden"/>
        <div id="date-picker-container"></div>
        <label for="cid">Company ID</label>
        <input type="number" class="form-control" min="1" name="cid" id="cid">
    </div>
</div>