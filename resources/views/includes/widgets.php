<div class="row widgets">
    <div class="col-md-4">
        <div id="email-stats" class="email-widget-block active">
            <div class="row description">
                <div class="col-xs-8">
                    <div class="widget-title">
                        Email stats
                    </div>
                    <small>based on unique data</small>
                </div>
                <div class="col-xs-4 text-right">
                    <div class="widget-open">Open: <strong>0.00%</strong></div>
                    <div class="widget-ctr">CTR: <strong>0.00%</strong></div>
                </div>
            </div>
            <ul class="row counters">
                <li class="col-xs-4 text-center">
                    <div class="counter sent-counter">0</div>
                    <small>sent</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter open-counter">0</div>
                    <small>opened</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter click-counter">0</div>
                    <small>clicked</small>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-4">
        <div id="broadcast-stats" class="email-widget-block">
            <div class="row description">
                <div class="col-xs-8">
                    <div class="widget-title">
                        Broadcast stats
                    </div>
                    <small>based on unique data</small>
                </div>
                <div class="col-xs-4 text-right">
                    <div class="widget-open">Open: <strong>0.00%</strong></div>
                    <div class="widget-ctr">CTR: <strong>0.00%</strong></div>
                </div>
            </div>
            <ul class="row counters">
                <li class="col-xs-4 text-center">
                    <div class="counter sent-counter">0</div>
                    <small>sent</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter open-counter">0</div>
                    <small>opened</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter click-counter">0</div>
                    <small>clicked</small>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-4">
        <div id="rss-broadcast-stats" class="email-widget-block">
            <div class="row description">
                <div class="col-xs-8">
                    <div class="widget-title">
                        RSS Broadcast stats
                    </div>
                    <small>based on unique data</small>
                </div>
                <div class="col-xs-4 text-right">
                    <div class="widget-open">Open: <strong>0.00%</strong></div>
                    <div class="widget-ctr">CTR: <strong>0.00%</strong></div>
                </div>
            </div>
            <ul class="row counters">
                <li class="col-xs-4 text-center">
                    <div class="counter sent-counter">0</div>
                    <small>sent</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter open-counter">0</div>
                    <small>opened</small>
                </li>
                <li class="col-xs-4 text-center">
                    <div class="counter click-counter">0</div>
                    <small>clicked</small>
                </li>
            </ul>
        </div>
    </div>
</div>