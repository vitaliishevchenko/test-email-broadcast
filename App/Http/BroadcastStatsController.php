<?php

namespace App\Http;

use App\Models\Broadcast;

class BroadcastStatsController extends WidgetStatsController
{
    /**
     * Ajax endpoint method for Broadcasts data
     *
     * @return void
     */
    public function get_xhr()
    {
        $data = $this->prepareWidget(Broadcast::class);

        $this->responseXhr($data);
    }
}