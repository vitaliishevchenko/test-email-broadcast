<?php

if (! function_exists('view')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param null $key
     *
     * @return string
     */
    function view($key = null)
    {
        if (is_null($key)) {
            return '';
        }

        require_once __DIR__.'/../resources/views/'.$key.'.php';
    }
}

if (! function_exists('array_pluck')) {
    /**
     * Pluck an array of values from an array.
     *
     * @param $array
     * @param $key
     *
     * @return array
     */
    function array_pluck($array, $key)
    {
        return array_map(function ($v) use ($key) {
            return is_object($v) ? $v->$key : $v[$key];
        }, $array);
    }
}