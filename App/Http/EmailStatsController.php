<?php

namespace App\Http;

use App\Models\Email;

class EmailStatsController extends WidgetStatsController
{
    /**
     * Ajax endpoint method for Emails data
     *
     * @return void
     */
    public function get_xhr()
    {
        $data = $this->prepareWidget(Email::class);

        $this->responseXhr($data);
    }
}