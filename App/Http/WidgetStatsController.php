<?php
/**
 * Created by PhpStorm.
 * User: will
 * Date: 16.11.17
 * Time: 12:15
 */

namespace App\Http;

class WidgetStatsController extends Controller
{
    /**
     * Fetch widget data for response
     *
     * @param      $className
     * @param bool $rss
     *
     * @return mixed
     */
    public function prepareWidget($className, $rss = false)
    {
        $startDate = $this->getStartDate();
        $endDate = $this->getEndDate();
        $cid = $this->getCompanyId();
        $excludedIds = $this->getExcludedIds();

        $model = new $className($startDate, $endDate, $excludedIds, $cid, $rss);

        if (isset($_GET['fetch'])) {
            $data['sent_emails'] = $model->getSentEmails();
            $data['open_emails'] = $model->getOpenedEmails();
            $data['click_emails'] = $model->getClickedEmails();
        } else {
            $data['sent_count'] = $model->countSentEmails();
            $data['open_count_unique'] = $model->countOpenedEmails(true);
            $data['open_count'] = $model->countOpenedEmails();
            $data['click_count_unique'] = $model->countClickedEmails(true);
            $data['click_count'] = $model->countClickedEmails();
        }

        return $data;
    }
}