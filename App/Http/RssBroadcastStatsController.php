<?php

namespace App\Http;

use App\Models\Broadcast;

class RssBroadcastStatsController extends WidgetStatsController
{
    /**
     * Ajax endpoint method for RSS Broadcasts data
     *
     * @return void
     */
    public function get_xhr()
    {
        $data = $this->prepareWidget(Broadcast::class, true);

        $this->responseXhr($data);
    }
}