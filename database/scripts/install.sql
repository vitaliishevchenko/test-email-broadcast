CREATE TABLE `emails` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`cid`  int(11) NOT NULL ,
`pid`  int(11) NULL DEFAULT NULL ,
`sender_profile_id`  int(11) NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`subject`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`altSubject`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`body`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`date`  bigint(20) NULL DEFAULT NULL ,
`archive`  tinyint(4) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
INDEX `cid` (`cid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=218
ROW_FORMAT=COMPACT
;


CREATE TABLE `broadcasts` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`cid`  int(11) NULL DEFAULT NULL ,
`pid`  int(11) NULL DEFAULT NULL ,
`email_id`  int(11) NULL DEFAULT NULL ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sent_to`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`send_type`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`send_timing`  bigint(11) NULL DEFAULT NULL ,
`btype`  varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`rss_url`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`rss_last_sent`  bigint(20) NULL DEFAULT NULL ,
`date`  bigint(20) NULL DEFAULT NULL ,
`cronned`  tinyint(4) NOT NULL DEFAULT 0 ,
`excluded_list`  longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`archive`  tinyint(4) NOT NULL DEFAULT 0 ,
`sender_profile_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `cid` (`cid`) USING BTREE ,
INDEX `email_id` (`email_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=36
ROW_FORMAT=COMPACT
;




CREATE TABLE `email_opens` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`email_id`  int(11) NULL DEFAULT NULL ,
`cc_id`  int(11) NULL DEFAULT NULL ,
`his_id`  int(11) NULL DEFAULT NULL ,
`date`  bigint(20) NULL DEFAULT NULL ,
`service_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `email id` (`email_id`) USING BTREE ,
INDEX `contact id` (`cc_id`) USING BTREE ,
INDEX `service email sent from` (`service_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=4446
ROW_FORMAT=COMPACT
;


CREATE TABLE `email_clicks` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`email_id`  int(11) NULL DEFAULT NULL ,
`cc_id`  int(11) NULL DEFAULT NULL ,
`url`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`date`  bigint(20) NULL DEFAULT NULL ,
`his_id`  int(11) NULL DEFAULT NULL ,
`service_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `contact` (`cc_id`) USING BTREE ,
INDEX `email` (`email_id`) USING BTREE ,
INDEX `service email sent form` (`service_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=685
ROW_FORMAT=COMPACT
;

CREATE TABLE `reports_email_history` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`date`  bigint(20) NULL DEFAULT NULL ,
`email_id`  int(11) NULL DEFAULT NULL ,
`send_time`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`timezone`  tinyint(4) NOT NULL ,
`source`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`source_id`  int(11) NULL DEFAULT NULL ,
`sub_source_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `email id` (`email_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=5162
ROW_FORMAT=COMPACT
;


CREATE TABLE `queue_emails` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`his_id`  int(11) NULL DEFAULT NULL ,
`email_id`  int(11) NULL DEFAULT NULL ,
`cc_id`  int(11) NULL DEFAULT NULL ,
`cinfo_id`  int(11) NULL DEFAULT NULL ,
`service`  int(11) NULL DEFAULT NULL ,
`date`  int(11) NULL DEFAULT NULL ,
`sent`  tinyint(4) NOT NULL ,
`segments`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tags`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `report history id` (`his_id`) USING BTREE ,
INDEX `contact id` (`cc_id`) USING BTREE ,
INDEX `email id` (`email_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=9701
ROW_FORMAT=COMPACT
;




